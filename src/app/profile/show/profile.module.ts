import { NgModule } from '@angular/core';
import { NgZorroAntdModule, NZ_I18N, es_ES } from 'ng-zorro-antd';
import { ProfileComponent } from './profile.component'
import { ProfileRoutingModule } from './profile-routing.module';

@NgModule({
  imports: [NgZorroAntdModule,ProfileRoutingModule],
  declarations: [ProfileComponent],
  exports: [ProfileComponent],
  providers: [{ provide: NZ_I18N, useValue: es_ES }]
})
export class ProfileModule { }
