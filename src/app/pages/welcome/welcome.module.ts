import { NgModule } from '@angular/core';
import { WelcomeRoutingModule } from './welcome-routing.module';
import { WelcomeComponent } from './welcome.component';
import { NgZorroAntdModule, NZ_I18N, es_ES } from 'ng-zorro-antd';


@NgModule({
  imports: [NgZorroAntdModule,WelcomeRoutingModule],
  declarations: [WelcomeComponent],
  exports: [WelcomeComponent],
  providers: [{ provide: NZ_I18N, useValue: es_ES }]
})
export class WelcomeModule { }
