import { NgModule } from '@angular/core';
import { ShowRoutingModule } from './show-routing.module';
import { NgZorroAntdModule, NZ_I18N, es_ES } from 'ng-zorro-antd';
import { ShowComponent } from './show.component'

@NgModule({
  imports: [NgZorroAntdModule,ShowRoutingModule],
  declarations: [ShowComponent],
  exports: [ShowComponent],
  providers: [{ provide: NZ_I18N, useValue: es_ES }]
})
export class ShowModule { }
